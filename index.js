//alert("Hello, 176!")

//Mini Activity:
/*
	How do we display tasks in the console.

	drink html
	eat javascript
	inhale css
	bake bootstrap

	Send a screenshot in our batch hangouts.

*/

let task1 = "drink html"
let task2 = "eat javascript"
let task3 = "inhale css"
let task4 = "bake bootsrap"
console.log(task1)
console.log(task2)
console.log(task3)
console.log(task4)

let myTasks = ["drink html",  "eat javascript","inhale css", "bake bootsrap" ]
console.log(myTasks)

/*
	Arrays are used to store multiple related values in a single variable
	They are declared using square bracket[] also known as "Array Literals"

	Syntax:
		let/const arrayName = [elementA, elementB, elementC ... ]
	
*/

//Common Examples of Arrays
let grades = [98.5, 94.3, 89.2, 90.1]
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"]
let mixedArr = [12, 'Asus', null, undefined, {}]; 
console.log(grades)
console.log(computerBrands)
console.log(mixedArr)

//Reassigning array values
console.log(myTasks)
//arrayName[index]
myTasks[0] = "hello world";
console.log(myTasks)

console.log(grades[1]) //94.3
console.log(computerBrands[6]) //Toshiba
console.log(computerBrands[10]) //undefined

//How do we get the number of elements in our array?
console.log(computerBrands.length) //8

if (computerBrands.length > 5) {
	console.log("Too many Suppliers!")
}

//How do we access the last element of our array?
let lastElementIndex = computerBrands.length -1
console.log(computerBrands[lastElementIndex])

//Array Methods
/*
	push()
		- Adds an element in the end of our array and returns the array's length

	Syntax:
		arrayName.push()
*/

let fruits = ["Apple", "Orange", "Kiwi", "Grapes", "Dragon Fruit"]
console.log(fruits)

let fruitsLength = fruits.push("Mango");
console.log(fruitsLength);
console.log(fruits)

//Adding multiple elements in our array
fruits.push("Avocado", "Guava")
console.log(fruits)

/*
	pop()
		- removes the last element in our array and returns the removed element

	Syntax:
		arrayName.pop()
*/

let removedFruit = fruits.pop()
console.log(removedFruit)
console.log(fruits)

/*
	unshift()
		- Adds one or more elements at the beginning of an array

	Syntax:
		arrayName.unshift("elementA")
		arrayName.unshift("elementA", "elementB")
*/

fruits.unshift("Lime", "Banana")
console.log(fruits)

/*
	shift()
		- removes an element at the beginning of an array
	
	Syntax:
		arrayName.shift()
*/

let anotherFruit = fruits.shift()
console.log(anotherFruit)
console.log(fruits)


/*
    splice()

        -Simultaneously removes an element from a specified index number and add an element.

        syntax:
            arrayName.splice(startingIndex, deleteCount, elementToBeAdded)


*/

fruits.splice(1, 2, 'Cherry', 'Peach', 'Strawberry', 'Durian', 'Lansones' )
console.log(fruits)
console.log(fruits.length)

/*

    sort()
        -Rearranges the array elements in alphanumeric order

            syntax:

            arrayName.sort()


*/

fruits.sort()
console.log(fruits)

let arrayWithNumber = [1 , 2, 'Cherry', 'Strawberry', 'Durian', 5]
arrayWithNumber.sort()
console.log(arrayWithNumber)

/*
    reverse()
        -Reverses the order of array elements
        last to first and first to last reversing effect

            syntax:
            arrayName.reverse()


*/

fruits.reverse()
console.log(fruits)

/*
    indexOf()
        -Returns the index number of the first matching element fount in an array. Returns -1 if no match found.

        syntax:
            array.indexOf(searchValue)
            array.indexOf(searchValue, fromIndex)

*/

let countries = ['USA', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'KR',]

let firstIndex = countries.indexOf('PH')
console.log(firstIndex)

let invalidCountry = countries.indexOf('RU')
console.log(invalidCountry)

/*
    lastIndex()
        - Return the index number of the last matching element found in our array.
        - The search process will be done from last element proceeding to the first element.

        Syntax:
            arrayName.lastIndexOf(searchValue)
            arrayname.lastIndexOf(searchValue, fromIndex)

*/

let lastIndex = countries.lastIndexOf('PH')
console.log(lastIndex)


/*
    slice()

        -Portions/slices elements from an array and returns a newa array.

        syntax:
        arrayName.slice(startingIndex)
        arrayName.slice(startingIndex, endingIndex)


*/

let slicedArrayA = countries.slice(2)
console.log(slicedArrayA)

let slicedArrayB = countries.slice(2,4)
console.log(slicedArrayB)

/*
    toString()

    -Returns an array as a string separated by commas.

    Syntax:
        arrayName.toString()
*/

let stringArray = countries.toString()
console.log(stringArray)

/*
    concat()
    -combines two arrays and returns the combined results.

    Syntax:
        arrayA.concat(arrayB)
        arrayA.concat(arrayB, arrayC, ...)

*/

let taskArrayA = ['eat javascript', 'drink html']
let taskArrayB = ['inhale css', 'breathe sass']
let taskArrayC = ['get git', 'be node']

let tasks = taskArrayA.concat(taskArrayB)
console.log(tasks)

let allTask = taskArrayA.concat(taskArrayB, taskArrayC)
console.log(allTask)

/*
combining arrays with elements

*/

let combinedTasks = taskArrayA.concat('smell express', 'throw react')
console.log(combinedTasks)

/*
    join()

    -Returns an array as a string separated by specified separator string

    Syntax:

        arrayName.join('separatorString')
*/

let users = ['john', 'jane', 'june', 'sean' ];
console.log(users.join())
console.log(users.join(' '))
console.log(users.join('-'))

/*
ITERATION METHOD

        forEach

        -similar to a for loop that iterates on each array element.

        syntax:
        arrayName.forEach(function(indivElement)) {

        }

*/

allTask.forEach(function(task){
    console.log(task)
})

/*

forEach with conditional statement

*/

let filteredTasks = [];

allTask.forEach(function(task) {

    if(task.length > 10){
        filteredTasks.push(task)

    }
})

console.log(filteredTasks)


/*

    map()
        -Iterates on each element and returns a new array with a different value depending on the result of the function's operation.

        syntax:
        let/const resultArray = arrayName.map(function(individualElement){
            statement
        })

*/

let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number){
    return number * number
})

console.log(numberMap)

/*
    every()
        -Checks if all elements in an array met the given condition.

        syntax:

        let/const resultArray = arrayName.every(function(individualElement){
            return (expression/condition) t or f
        })

*/

let allValid = numbers.every(function(number){
    return (number < 6)
})

console.log(allValid)

/*
    some()
        -checks if atleast one element met the given condition
        -returns true if at least one element met the given condition, and false if not

        syntax:

        let/const resultArray = arrayName arrayName.some(function(individualElement){
            return (expression/condition) t or f
        })
*/

let someValid = numbers.some(function(number){
    return (number <2)
})

console.log(someValid)


/*
    filter()
    -returns new array that contains elements which met the given condition

    syntax:
    let/const resultArray = arrayName.filter(function(individualElement){

    })
*/

let filterValid = numbers.filter(function(number){
    return (number < 3)
})

console.log(filterValid)

let nothingFound = numbers.filter(function(number){
    return (number == 0)
})

console.log(nothingFound)


//filtering for each

let filteredNumber = []

numbers.forEach(function(number){
    if(number < 3){
        filteredNumber.push(number)
    }
})

console.log(filteredNumber)

/*
    includes()


*/

products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor',]

let filteredProducts = products.filter(function(product){
    return product.toLowerCase().includes('a')

})
console.log(filteredProducts)

//Multidimensional Arrays

/*
    Multidimensional arrays are useful for storing complex data structures
*/

let chessBoard = [
    ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', ],

    ['b1', 'b2', 'b3', 'b4', 'b5', 'b6', 'b7', 'b8', 'b9', ],

    ['c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9', ],

    ['d1', 'd2', 'd3', 'd4', 'd5', 'd6', 'd7', 'd8', 'd9', ],

    ['e1', 'e2', 'e3', 'e4', 'e5', 'e6', 'e7', 'e8', 'e9', ],

    ['f1', 'f2', 'f3', 'f4', 'f5', 'f6', 'f7', 'f8', 'f9', ],

    ['g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'g7', 'g8', 'g9', ],
]

console.log(chessBoard)
console.log(chessBoard[4][6])